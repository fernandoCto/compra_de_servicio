<?php

namespace Cps\Administracion\AdministracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="adm_gerenciaAdm")
 * @ORM\Entity()
 */
class Gerenciaadm{

    public function __construct(){
        $this->empresas = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $sigla;
    
// === Foraneas ======================================================== //

    /**
    * @ORM\OneToMany(targetEntity="Cps\Afiliacion\AfiliacionBundle\Entity\Empresa", mappedBy="gerenciaAdmPago")
    */
    protected $pagoEmpresas;    
    
// === Funciones Auxiliares ============================================ //

    public function __toString(){
        return $this->getNombre();
    }
       
// === Getter ========================================================= //

    /**
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return string 
     */
    public function getNombre(){
        return $this->nombre;
    }

    /**
     * @return string 
     */
    public function getSigla(){
        return $this->sigla;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagoEmpresas(){
        return $this->pagoEmpresas;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Gerenciaadm
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Set sigla
     *
     * @param string $sigla
     * @return Gerenciaadm
     */
    public function setSigla($sigla)
    {
        $this->sigla = $sigla;

        return $this;
    }

    /**
     * Add pagoEmpresas
     *
     * @param \Cps\Afiliacion\AfiliacionBundle\Entity\Empresa $pagoEmpresas
     * @return Gerenciaadm
     */
    public function addPagoEmpresa(\Cps\Afiliacion\AfiliacionBundle\Entity\Empresa $pagoEmpresas)
    {
        $this->pagoEmpresas[] = $pagoEmpresas;

        return $this;
    }

    /**
     * Remove pagoEmpresas
     *
     * @param \Cps\Afiliacion\AfiliacionBundle\Entity\Empresa $pagoEmpresas
     */
    public function removePagoEmpresa(\Cps\Afiliacion\AfiliacionBundle\Entity\Empresa $pagoEmpresas)
    {
        $this->pagoEmpresas->removeElement($pagoEmpresas);
    }
}
