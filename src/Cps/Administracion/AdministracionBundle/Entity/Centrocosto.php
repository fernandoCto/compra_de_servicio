<?php

namespace Cps\Administracion\AdministracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="adm_centroCosto",
              uniqueConstraints={@ORM\UniqueConstraint(name="duplicado_idx", columns={"sucursal_id", "ubicacion_id", "Servicio_id", "distintivo"})}
             )
 * @ORM\Entity(repositoryClass="Cps\Administracion\AdministracionBundle\Repository\CentrocostoRepository")
 */
class Centrocosto{

    public function __construct(){
        $this->solserexas = new ArrayCollection();
        $this->pruebas = new ArrayCollection();
        $this->tecnicos = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=15, nullable=false)
     */
    private $distintivo;

    /**
     * @ORM\Column(name="siglaDis", type="string", length=5, nullable=false)
     */
    private $siglaDis;

// === Otras Funciones ================================================= //

    public function __toString(){
        return $this->getId().":".$this->sucursal->getNombre().":".$this->ubicacion->getNombre().":".$this->Servicio->getNombre().":".$this->distintivo;
    }

    public function getNombre(){
        return $this->sucursal->getNombre().":".$this->ubicacion->getNombre().":".$this->Servicio->getNombre().":".$this->distintivo;
    }

    public function getSigla(){
        return $this->sucursal->getSigla().":".$this->ubicacion->getSigla().":".$this->Servicio->getSigla().":".$this->distintivo;
    }

// === Foraneas ======================================================== //

   /**
    * @ORM\ManyToOne(targetEntity="Sucursal", inversedBy="centroCostos")
    * @ORM\JoinColumn(nullable=false)
    */
   protected $sucursal;

   /**
    * @ORM\ManyToOne(targetEntity="Ubicacion", inversedBy="centroCostos")
    * @ORM\JoinColumn(nullable=false)
    */
   protected $ubicacion;

   /**
    * @ORM\ManyToOne(targetEntity="Servicio", inversedBy="centroCostos")
    * @ORM\JoinColumn(nullable=false)
    */
   protected $servicio;

   /**
 	 *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\user", mappedBy="Centrocosto")
 	 *@ORM\JoinColumn(nullable=false)
 	 */
 	 protected $user;
   
   /**
 	 *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\jefatura", mappedBy="centrocosto")
 	 *@ORM\JoinColumn(nullable=false)
 	 */
 	 protected $jefatura;

// === Getter ============================================================ //

    /**
     * @return integer
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDistintivo(){
        return $this->distintivo;
    }

    /**
     * @return string
     */
    public function getSiglaDis(){
        return $this->siglaDis;
    }

    /**
     * @return \Cps\Administracion\AdministracionBundle\Entity\Sucursal
     */
    public function getSucursal(){
        return $this->sucursal;
    }

    /**
     * @return \Cps\Administracion\AdministracionBundle\Entity\Ubicacion
     */
    public function getUbicacion(){
        return $this->ubicacion;
    }

    /**
     * @return \Cps\Administracion\AdministracionBundle\Entity\Servicio
     */
    public function getServicio()
    {
        return $this->servicio;
    }

// GET2

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSolserexas(){
        return $this->solserexas;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPruebas(){
        return $this->pruebas;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTecnicos(){
        return $this->tecnicos;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers(){
        return $this->users;
    }
}
