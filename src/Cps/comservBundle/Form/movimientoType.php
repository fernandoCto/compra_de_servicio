<?php

namespace Cps\comservBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class movimientoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('origenServ')->add('destinoServ')->add('fechaEnvio')->add('fechaRecibido')->add('usuEnvio')->add('usuRecibido')->add('observacion')->add('cartaRef')->add('estadoSol')->add('estadoReg')        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cps\comservBundle\Entity\movimiento'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'cps_comservbundle_movimiento';
    }


}
