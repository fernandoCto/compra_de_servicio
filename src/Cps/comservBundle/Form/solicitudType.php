<?php

namespace Cps\comservBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class solicitudType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha', 'genemu_jquerydate', array(
                'widget' => 'single_text',
                'data' => new \DateTime("now")
            ))
            ->add('requerimiento')
            ->add('diagnostico', 'textarea')
            ->add('medicoId',  'genemu_jqueryautocomplete_entity', array(
                'class' => 'Cps\Personal\ArchivoBundle\Entity\Empleado',
                'property' => 'busajax','label' => 'Medico',
            ))
            ->add('tipoSol');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cps\comservBundle\Entity\solicitud'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'cps_comservbundle_solicitud';
    }


}
