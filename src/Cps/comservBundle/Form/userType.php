<?php
namespace Cps\comservBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormInterface;
use cpscpraservBundle\Entity\branch;

class userType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name')
			->add('lastName')
			->add('email')
			->add('username')
			->add('password')
			->add('role', 'choice', array(
			'choices' => array(
				'ROLE_ADMIN' => 'Administrador',
				'ROLE_USER' => 'Usuario',
                'ROLE_HOS' => 'Hospital',
                'ROLE_POL' => 'Policonsultorio',
                'ROLE_REG' => 'Regional'
			)
		))
			->add('isActive', 'checkbox');
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Cps\comservBundle\Entity\user'
		));
	}
}
