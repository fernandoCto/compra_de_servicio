<?php

namespace Cps\comservBundle\Controller;

use Cps\comservBundle\Entity\tipo_sol;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Tipo_sol controller.
 *
 * @Route("tipo_sol")
 */
class tipo_solController extends Controller
{
    /**
     * Lists all tipo_sol entities.
     *
     * @Route("/", name="tipo_sol_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tipo_sols = $em->getRepository('cpscomservBundle:tipo_sol')->findAll();

        return $this->render('tipo_sol/index.html.twig', array(
            'tipo_sols' => $tipo_sols,
        ));
    }

    /**
     * Creates a new tipo_sol entity.
     *
     * @Route("/new", name="tipo_sol_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $tipo_sol = new Tipo_sol();
        $form = $this->createForm('Cps\comservBundle\Form\tipo_solType', $tipo_sol);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tipo_sol);
            $em->flush($tipo_sol);

            return $this->redirectToRoute('tipo_sol_index');
        }

        return $this->render('tipo_sol/new.html.twig', array(
            'tipo_sol' => $tipo_sol,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tipo_sol entity.
     *
     * @Route("/{id}", name="tipo_sol_show")
     * @Method("GET")
     */
    public function showAction(tipo_sol $tipo_sol)
    {
        $deleteForm = $this->createDeleteForm($tipo_sol);

        return $this->render('tipo_sol/show.html.twig', array(
            'tipo_sol' => $tipo_sol,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tipo_sol entity.
     *
     * @Route("/{id}/edit", name="tipo_sol_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, tipo_sol $tipo_sol)
    {
        $deleteForm = $this->createDeleteForm($tipo_sol);
        $editForm = $this->createForm('Cps\comservBundle\Form\tipo_solType', $tipo_sol);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tipo_sol_edit', array('id' => $tipo_sol->getId()));
        }

        return $this->render('tipo_sol/edit.html.twig', array(
            'tipo_sol' => $tipo_sol,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tipo_sol entity.
     *
     * @Route("/{id}", name="tipo_sol_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, tipo_sol $tipo_sol)
    {
        $form = $this->createDeleteForm($tipo_sol);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tipo_sol);
            $em->flush($tipo_sol);
        }

        return $this->redirectToRoute('tipo_sol_index');
    }

    /**
     * Creates a form to delete a tipo_sol entity.
     *
     * @param tipo_sol $tipo_sol The tipo_sol entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(tipo_sol $tipo_sol)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipo_sol_delete', array('id' => $tipo_sol->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
