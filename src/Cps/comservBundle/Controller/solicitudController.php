<?php

namespace Cps\comservBundle\Controller;

use Cps\comservBundle\Entity\solicitud;
use Cps\comservBundle\Entity\bitacorap;
use Cps\comservBundle\Entity\bitacorat;
use Cps\comservBundle\Entity\movimiento;
use Cps\comservBundle\Entity\factura;
use Cps\comservBundle\Entity\proforma;
use Cps\Personal\ArchivoBundle\Entity\Empleado;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * solicitud controller.
 *
 * @Route("solicitud")
 */
class solicitudController extends Controller
{
    function getUsuario()
    {
        $usuario = $this->get('security.context')->getToken()->getUser();
        return $usuario;
    }
    function getServicio()
    {
        $usuario = $this->get('security.context')->getToken()->getUser()->getCentrocosto()->getServicio()->getId();
        return $usuario;
    }
    public function getOrigen(){
      $ubicacion = $this->get('security.context')->getToken()->getUser()->getCentrocosto()->getUbicacion();
//      switch ($ubicacion->getId()) {
//        case 1:
//          $origen = 'A';
//          break;
//        case 2:
//          $origen = 'H';
//          break;
//            dump("Se ha producido un Error #56231, contactese con Ronald Aguilar 8054");die;
//            break;
//          default:
//          case 3:
//          $origen = 'P';
//          break;
//      }
      $origen = $ubicacion->getId();
      return $origen;
    }

    /**
     * Lists all solicitud entities.
     *
     * @Route("/listado/{est}", name="solicitud_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, $est)
    {
        $BuscarNombre = $request->get('query');

        $usuario = $this->getUsuario()->getId();
        $em = $this->getDoctrine()->getManager();
        $solicitudes = $em->getRepository('cpscomservBundle:solicitud')->BuscarSolicitudesPorPaciente($usuario, $BuscarNombre, $est);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($solicitudes, $request->query->getInt('page', 1),11);

        $deleteFormAjax = $this->createCustomForm(':USER_ID', 'DELETE', 'solicitud_delete');
        return $this->render('solicitud/index.html.twig', array(
            'pagination' => $pagination,
            'delete_form_ajax' => $deleteFormAjax->createView()
        ));
    }

    /**
     * Creates a new solicitud entity.
     *
     * @Route("/new", name="solicitud_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $Origen = $this->getOrigen();
        $session = $this->getRequest()->getSession();
        $mat = $session->get('mat'); // Matricula del paciente por session
        $matAseg = $session->get('matase'); // Matricula del titular por session
        $tipo = $session->get('tipo');
        $empresaCod = $session->get('empresaCod');
        $nomPaciente = $session->get('nomCom');
        $nomAsegurado = $session->get('nomComAseg');

        $sexo = 'Masculino';
        $dig = substr($mat, 4, 1);
        if ( $dig > 4 ) $sexo = "Femenino";
        $dia = substr($mat,6,2);
        $mes = substr($mat,4,2);
        if ($mes > 50) $mes = $mes - 50;
        $ano = substr($mat,0,4);
        $fchNacLit = $ano.'-'.$mes.'-'.$dia;
        $fchNac = new \Datetime($fchNacLit);
        $edad = date_diff($fchNac, new \Datetime());
        $esNino = false;
        if ($edad->format('%y%') <= 14 ) $esNino = true;
        $fechNac = $ano."-".$mes."-".$dia;

        ///////////////////////////FECHA ACTUAL///////////////////////////////////

        $fchAct = new \Datetime("now");
        $ano1 = $fchAct->format("Y");
        $mes1 = $fchAct->format("m");
        $dia1 = $fchAct->format("d");

        $timestamp1 = mktime(0,0,0,$mes,$dia,$ano);
        $timestamp2 = mktime(4,12,0,$mes1,$dia1,$ano1);
        $segundos_diferencia = $timestamp1 - $timestamp2;
        $dias_diferencia = $segundos_diferencia / (60 * 60 * 24);
        $dias_diferencia = abs($dias_diferencia);
        $dias_diferencia = floor($dias_diferencia);
        $edadf= $edad->format('%y%');
        if ($dias_diferencia <= 28) {
            $edadf=500;
        }
        ///////////////////////////// SOLICITUD //////////////////////////////////////
        $solicitud = new Solicitud();
        $solicitud->setPacienteId($mat);
        $solicitud->setEmpresaCod($empresaCod);
        $solicitud->setTipoPaciente($tipo);
        $solicitud->setOrigen($Origen);
        $solicitud->setEstadoSolicitud('AC');
        $solicitud->setUser($this->getUser());
//         dump($solicitud);die;
        $form = $this->createForm('Cps\comservBundle\Form\solicitudType', $solicitud);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($tipo == "BE") {
              $bitacorat = $em->getRepository('cpscomservBundle:bitacorat')->findOneByMatricula($matAseg);
              if ($bitacorat == null) {
                $bitacorat = new bitacorat();
                $bitacorat->setMatricula($matAseg);
                $bitacorat->setNombre($nomAsegurado);
                $em->persist($bitacorat);
                $em->flush($bitacorat);
                $bitacorat_id = $bitacorat->getId();
              }else{
                $bitacorat_id = $bitacorat->getId();
              }
            } else {
              $bitacorat_id = null;
              $bitacorat = null;
            }
            $bitacorap = $em->getRepository('cpscomservBundle:bitacorap')->findOneBy(
              array(
                'matriculap' => $mat,
                'bitacorat' => $bitacorat_id
              )
            );
            if ($bitacorap == NULL) {
              $bitacorap = new bitacorap();
              $bitacorap->setMatriculap($mat);
              $bitacorap->setTipo($tipo);
              $bitacorap->setNombrep($nomPaciente);
              $bitacorap->setBitacorat($bitacorat);
              $em->persist($bitacorap);
              $em->flush($bitacorap);
            }
            $empresa = $em->getRepository('CpsAfiliacionBundle:Empresa')->findOneByCod($empresaCod);
            /*proceso*/
            $procesoInicial = $em->getRepository('cpscomservBundle:proceso')->find(1);
            /*proceso*/

            $solicitud->setEmpresa($empresa);
            $solicitud->setBitacorap($bitacorap);
            $solicitud->setProceso($procesoInicial);
            // dump($solicitud);die;
            $em->persist($solicitud);
            $em->flush($solicitud);

            //return $this->redirectToRoute('solicitud_show', array('id' => $solicitud->getId()));
            return $this->redirectToRoute('solicitud_index', array('est'=> 1));

        }
        return $this->render('solicitud/new.html.twig', array(
            'solicitud' => $solicitud,
            'sexo' => $sexo,
            'edadf' => $edadf,
            'tipo'=> $tipo,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a solicitud entity.
     *
     * @Route("/{id}/{est}", name="solicitud_show")
     * @Method({"GET","POST"})
     */
    public function showAction(solicitud $solicitud , Request $request)
    {
        $deleteForm = $this->createDeleteForm($solicitud);
//        DATOS DE SESSION
        $usuarioId = $this->getUsuario()->getId();
        $servicioId = $this->getServicio();

//        FORMULARIO DE PROFORMA
        $proforma = new Proforma();
        $proforma->setSolicitud($solicitud);

        $formProforma = $this->createForm('Cps\comservBundle\Form\proformaType', $proforma);
        $formProforma->handleRequest($request);

        if ($formProforma->isSubmitted() && $formProforma->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($proforma);
            $em->flush($proforma);
            return $this->redirect($request->getUri());
        }

//        FORMULARIO DE FACTURA
        $factura = new Factura();
        $factura->setSolicitud($solicitud);

        $formFactura = $this->createForm('Cps\comservBundle\Form\facturaType', $factura);
        $formFactura->handleRequest($request);

        if ($formFactura->isSubmitted() && $formFactura->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($factura);
            $em->flush($factura);
            return $this->redirect($request->getUri());
        }

        $em = $this->getDoctrine()->getManager();
        $medico = $em->getRepository('CpsPerArchivoBundle:Empleado')->findOneById($solicitud->getMedicoId());
        $empresa = $em->getRepository('CpsAfiliacionBundle:Empresa')->nombreEmpresa($solicitud->getEmpresaCod());
        //recuperar datos

//        dump($solicitud);die;

        $servform = array('servicio' => null, 'observacion' => null);
        $form = $this->createFormBuilder($servform)
              ->add('servicio', 'choice',array('choices' => array(
                  "1"=>"Trabajo Social",
                  "2"=>"Dir. consulta externa",
                  )))
              ->add('observacion', 'text', array('label' => 'Observación '))
        ->getForm();
         $form->handleRequest($request);

//         FORMULARIO DE ENVIO
        if ($form->isSubmitted() && $form->isValid()) {
          $data = $form->getData();
          $servDestino = $data['servicio'];
          $observacion = $data['observacion'];
          // dump($data['observacion']);die;
          //creado un nuevo Movimiento
          $movimiento = new Movimiento();
          $movimiento->setUsuEnvio($usuarioId);
          $movimiento->setOrigenServ($servicioId);
          $movimiento->setDestinoServ($servDestino);
          $movimiento->setFechaEnvio(new \Datetime("now"));
          $movimiento->setEstadoReg(1);
          $movimiento->setEstadoSol(1);
          $movimiento->setObservacion($observacion);
          $movimiento->setSolicitud($solicitud);
          // dump($movimiento);die;
          //guardar
          $em = $this->getDoctrine()->getManager();
          $em->persist($movimiento);
          $em->flush($movimiento);

            return $this->redirect($request->getUri());
        }

//        MOVIMIENTO DE LA SOLICITUD
        $movimientoSolicitud = $em->getRepository('cpscomservBundle:movimiento')->findBySolicitud($solicitud->getId());
        $listaMovimiento = array();
        $cont = 0;
        foreach ( $movimientoSolicitud as $fila){
            $listaMovimiento[$cont]['id'] = $fila->getId();

            $origenServicio = $em->getRepository('CpsAdministracionBundle:servicio')->findOneById($fila->getOrigenServ());
            $listaMovimiento[$cont]['origenServ'] = $origenServicio->getNombre();

            $listaMovimiento[$cont]['fechaEnvio'] = $fila->getFechaEnvio();
            if ($fila->getDestinoServ() == 1){
                $destinoServicio = 'Trabajo Social';
            }else{
                $destinoServicio = 'Dir. Hospital';
            }
            $listaMovimiento[$cont]['destinoServ'] = $destinoServicio;
            $listaMovimiento[$cont]['fechaRecibido'] = $fila->getFechaRecibido();
            $listaMovimiento[$cont]['observacion'] = $fila->getObservacion();
            $listaMovimiento[$cont]['estadoSol'] = $fila->getEstadoSol();
            $cont++;
        }
//        dump($movimientos);die;

        return $this->render('solicitud/show.html.twig', array(
            'listaMovimiento'=> $listaMovimiento,
            'solicitud' => $solicitud,
            'medico' => $medico,
            'empresa' => $empresa,
            'form' => $form->createView(),
            'formFactura' => $formFactura->createView(),
            'formProforma' => $formProforma->createView(),
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Displays a form to edit an existing solicitud entity.
     *
     * @Route("/{id}/edit", name="solicitud_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, solicitud $solicitud)
    {
        $editForm = $this->createForm('Cps\comservBundle\Form\solicitudType', $solicitud);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('solicitud_edit', array('id' => $solicitud->getId()));
        }

        return $this->render('solicitud/edit.html.twig', array(
            'solicitud' => $solicitud,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a jefatura entity.
     *
     * @Route("/eliminar/{id}", name="solicitud_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, solicitud $solicitud)
    {
//        dump($solicitud);die;
        $BuscarNombre = $request->get('query');
        $usuario = $this->getUsuario()->getId();
        $est = 1;
        $form = $this->createCustomForm($solicitud->getId(), 'DELETE', 'solicitud_delete');
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $solicitudes = $em->getRepository('cpscomservBundle:solicitud')->BuscarSolicitudesPorPaciente($usuario, $BuscarNombre, $est);
        $totalSol = count($solicitudes);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($request->isXmlHttpRequest()){
                $removed = 1;
                $alert = 'error  ';
                $solicitud->setEstadoSolicitud('IN');
                $em->persist($solicitud);
                $em->flush($solicitud);
                return new Response(
                  json_encode(array(
                      'removed' => $removed,
                      'message' => $alert,
                      'totalSol' => $totalSol
                  )),200, array('Content-Type'=> 'application/json')
                );
            }
            $solicitud->setEstadoSolicitud('IN');
            $em->persist($solicitud);
            $em->flush($solicitud);
        }

        return $this->redirectToRoute('solicitud_index', array('est'=> 1));
    }

    private function createDeleteForm(solicitud $solicitud)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('solicitud_delete', array('id' => $solicitud->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
    private function createCustomForm($id, $method, $route)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm()
            ;
    }


    /**
     * reporte solicitud entity.
     *
     * @Route("/reporte", name="solicitud_reporte")
     * @Method({"GET", "POST"})
     */
    public function reporteAction()
    {
//        $anhoGestion = date("Y");
        $anhoGestion = 2017;

        $em = $this->getDoctrine()->getManager();
        $origen = $em->getRepository('cpscomservBundle:solicitud')->reporteOrigen();
        //dump($origen);die;
        $proceso = $em->getRepository('cpscomservBundle:solicitud')->reporteProceso();
        $dataGestion = $this->dataGestion($anhoGestion);

        return $this->render('solicitud/reporte/reporte.html.twig', array(
            'origen' => $origen,
            'proceso' => $proceso,
            'dataGestion' => $dataGestion,
            'anhoGestion' => $anhoGestion
        ));
    }

    public function dataGestion($anhoGestion)
    {
        $dataGestion = array();
        $con = $this->getDoctrine()->getEntityManager()->getConnection();
        $query = $con->executeQuery("SELECT YEAR(fecha) anho,MONTH(fecha) mes,COUNT(*) registro
                                            FROM COM_SERV_SOLICITUD
                                            WHERE YEAR(fecha) = $anhoGestion
                                            GROUP BY YEAR(fecha),MONTH(fecha)");
        foreach ($query->fetchAll() as $row){
            $data = $row['registro'];
            array_push($dataGestion ,$data);
//            dump($dataGestion);die;
        }
        return $dataGestion;
    }
}
