<?php

namespace Cps\comservBundle\Controller;

use Cps\comservBundle\Entity\factura;
use Cps\comservBundle\Entity\solicitud;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Factura controller.
 *
 * @Route("factura")
 */
class facturaController extends Controller
{
    /**
     * Lists all factura entities.
     *
     * @Route("/", name="factura_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, solicitud $solicitud)
    {
        $em = $this->getDoctrine()->getManager();

        $facturas = $em->getRepository('cpscomservBundle:factura')->findAll();

        return $this->render('factura/index.html.twig', array(
            'facturas' => $facturas,
        ));
    }

        /**
     * Lists all factura entities.
     *
     * @Route("/{id}/soli", name="factura_soli")
     * @Method("GET")
     */
    public function soliAction(Request $request, solicitud $solicitud)
    {
        $em = $this->getDoctrine()->getManager();
     //   $bitacorat = $em->getRepository('cpscomservBundle:bitacorat')->findOneByMatricula($matAseg);

      //dump($solicitud);die;
      $facturas = $em->getRepository('cpscomservBundle:factura')->findBySolicitud($solicitud);
     //   $facturas = $em->getRepository('cpscomservBundle:factura')->findAll();

        return $this->render('factura/soli.html.twig', array(
            'facturas' => $facturas,
            'solicitud' => $solicitud
        ));
    }

    /**
     * Creates a new factura entity.
     *
     * @Route("/new/{id}", name="factura_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, solicitud $solicitud)
    {
        $factura = new Factura();
        $factura->setSolicitud($solicitud);

        $form = $this->createForm('Cps\comservBundle\Form\facturaType', $factura);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($factura);
            $em->flush($factura);

            return $this->redirectToRoute('factura_show', array('id' => $factura->getId()));
        }

        return $this->render('factura/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a factura entity.
     *
     * @Route("/{id}", name="factura_show")
     * @Method("GET")
     */
    public function showAction(factura $factura)
    {
        $deleteForm = $this->createDeleteForm($factura);

        return $this->render('factura/show.html.twig', array(
            'factura' => $factura,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing factura entity.
     *
     * @Route("/{id}/edit", name="factura_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, factura $factura)
    {
        $deleteForm = $this->createDeleteForm($factura);
        $editForm = $this->createForm('Cps\comservBundle\Form\facturaType', $factura);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('factura_edit', array('id' => $factura->getId()));
        }

        return $this->render('factura/edit.html.twig', array(
            'factura' => $factura,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a factura entity.
     *
     * @Route("/{id}", name="factura_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, factura $factura)
    {
        $form = $this->createDeleteForm($factura);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($factura);
            $em->flush($factura);
        }

        return $this->redirectToRoute('factura_index');
    }

    /**
     * Creates a form to delete a factura entity.
     *
     * @param factura $factura The factura entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(factura $factura)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('factura_delete', array('id' => $factura->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
