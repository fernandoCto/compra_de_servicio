<?php

namespace Cps\comservBundle\Controller;

use Cps\comservBundle\Entity\jefatura;
use Cps\Administracion\AdministracionBundle\Entity\Centrocosto;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Jefatura controller.
 *
 * @Route("jefatura")
 */
class jefaturaController extends Controller
{
    /**
     * Lists all jefatura entities.
     *
     * @Route("/", name="jefatura_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $jefaturas = $em->getRepository('cpscomservBundle:jefatura')->findAll();

        return $this->render('jefatura/index.html.twig', array(
            'jefaturas' => $jefaturas,
        ));
    }

    /**
     * Creates a new jefatura entity.
     *
     * @Route("/new", name="jefatura_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $jefatura = new Jefatura();
        $form = $this->createForm('Cps\comservBundle\Form\jefaturaType', $jefatura);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($jefatura);
            $em->flush($jefatura);

            return $this->redirectToRoute('jefatura_show', array('id' => $jefatura->getId()));
        }

        return $this->render('jefatura/new.html.twig', array(
            'jefatura' => $jefatura,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a jefatura entity.
     *
     * @Route("/{id}", name="jefatura_show")
     * @Method("GET")
     */
    public function showAction(jefatura $jefatura)
    {
        $deleteForm = $this->createDeleteForm($jefatura);

        return $this->render('jefatura/show.html.twig', array(
            'jefatura' => $jefatura,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing jefatura entity.
     *
     * @Route("/{id}/edit", name="jefatura_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, jefatura $jefatura)
    {
        $deleteForm = $this->createDeleteForm($jefatura);
        $editForm = $this->createForm('Cps\comservBundle\Form\jefaturaType', $jefatura);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('jefatura_edit', array('id' => $jefatura->getId()));
        }

        return $this->render('jefatura/edit.html.twig', array(
            'jefatura' => $jefatura,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a jefatura entity.
     *
     * @Route("/{id}", name="jefatura_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, jefatura $jefatura)
    {
        $form = $this->createDeleteForm($jefatura);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($jefatura);
            $em->flush($jefatura);
        }

        return $this->redirectToRoute('jefatura_index');
    }

    /**
     * Creates a form to delete a jefatura entity.
     *
     * @param jefatura $jefatura The jefatura entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(jefatura $jefatura)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('jefatura_delete', array('id' => $jefatura->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
