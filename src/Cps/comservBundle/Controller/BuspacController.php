<?php

namespace Cps\comservBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Cps\Afiliacion\AfiliacionBundle\Form\BuspacType as Type;

/**
 * @Route("/buspac")
 */
class BuspacController extends Controller{
    
     public function __construct(){
        $this->rutBus = "";
        $this->rutDestino = "solicitud_new";
    }
    
    /**
     * @Route("/index", name="buscarpac")
     */
    public function indexAction(){
        $session = $this->getRequest()->getSession();
        $session->set('filtro', '');
        $session->set('mat', '');
		$session->set('matase', '');
        $session->set('esAseg', '');
        $session->set('apePat', '');
        $session->set('apeMat', '');
        $session->set('nom', '');
        $session->set('nomCom', '');
		$session->set('empresaId', '');
        $session->set('mensaje', "");
        $session->set('rutBus', $this->rutBus);
        $form = $this->createForm(new Type());
        $entities = array();
        return $this->render( "CpsAfiliacionBundle:Buspac:buscar2.html.twig", array('form' => $form->createView(), 'entities' => $entities));
    }
    
    /**
     * @Route("/index2", name="buscarpac2")
     */
    public function index2Action(){
        $session = $this->getRequest()->getSession();
        $filtro = $session->get('filtro');
        $session->set('mat', '');
		$session->set('matase', '');
        $session->set('esAseg', '');
        $session->set('apePat', '');
        $session->set('apeMat', '');
        $session->set('nom', '');
        $session->set('nomCom', '');
		$session->set('empresaId', '');
        $default = array('filtro'=>$filtro);
        $form = $this->createForm(new Type(), $default);
        $entities = array();
        return $this->render( "CpsAfiliacionBundle:Buspac:buscar2.html.twig", array('form' => $form->createView(),'entities' => $entities));
    }    
        
    /**
     * @Route("/procesar/filtropac", name="procesar_filtropac")
     * @Method("POST")
     */
    public function procesarfiltropacAction(Request $request){
        $session = $this->getRequest()->getSession(); $session->set('mensaje', ""); 
        $form = $this->createForm(new Type());
        $form->bind($request);
        $data = $request->request->get('cps_afiliacionbundle_buspactype');
   
        if ($data['filtro'] != null){
            $session->set('filtro', $data['filtro']);
            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository('CpsAfiliacionBundle:Aseg')->findpacXFiltro($data['filtro']);
            $entitiesA = $entities['aseg'];
            $entitiesB = $entities['bnef'];
            $totalA = count($entitiesA);
            $totalB = count($entitiesB);
            $total = $totalA + $totalB;
            if ($total > 0){
                if (count($entitiesA) == 1 and count($entitiesB) == 0)
                    return $this->redirect($this->generateUrl($this->rutBus.'procesarelepac', array( 'esAseg'=> 1, 'mat'=> $entitiesA[0]['cod'] ) ));
                elseif (count($entitiesA) == 0 and count($entitiesB) == 1)
                    return $this->redirect($this->generateUrl($this->rutBus.'procesarelepac', array( 'esAseg'=> 0, 'mat'=> $entitiesB[0]['cod'] ) ));
                elseif ($totalA > 50 OR $totalB > 50){
                    $session->set('mensaje', 'Muchos Registros encontrados ('.$total.'), restrinja su busqueda...');
                    return $this->redirect($this->generateUrl($this->rutBus.'buscarpac2'));
                }
            }else{
                $session->set('mensaje', 'No se encuentra PACIENTE con esos datos...');
                return $this->redirect($this->generateUrl($this->rutBus.'buscarpac2'));
            }
        }else{
            $session->set('mensaje', "Ingrese los datos...");
            return $this->redirect($this->generateUrl($this->rutBus.'buscarpac'));
        }
        $session->set('mensaje', "");
        return $this->render( "CpsAfiliacionBundle:Buspac:buscar2.html.twig", array('form' => $form->createView(), 'entities' => $entities));
    }

    /**
     * @Route("/procesarelepac/{esAseg}/{mat}", name="procesarelepac")
     */
    public function procesarelepacAction($esAseg, $mat){
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
       if ($esAseg){
            $entity = $em->getRepository('CpsAfiliacionBundle:Aseg')->findOneByCod($mat);
			$empresaId = $entity->getEmpresaId();
			$matase = '';
			$tipo = "AS";
        }else{   
            $entity = $em->getRepository('CpsAfiliacionBundle:Bnef')->findOneByCod($mat);
			$entityA = $em->getRepository('CpsAfiliacionBundle:Aseg')->findOneByCod($entity->getAseguradoId());
            $empresaId = $entityA->getEmpresaId();
			$matase = $entity->getAseguradoId();
			$tipo = "BE";
        }
        $datosAseg = null;
        $nomComAseg = null;
        if ($tipo == 'BE'){
            $datosAseg = $em->getRepository('CpsAfiliacionBundle:Aseg')->findOneByCod($matase);
        }
        $EntityEmpresa = $em->getRepository('CpsAfiliacionBundle:Empresa')->findOneByCod($empresaId);

        $nomCom = $entity->getApp()." ".$entity->getApm()." ".$entity->getNom();
        if ($datosAseg){
            $nomComAseg = $datosAseg->getApp()." ".$datosAseg->getApm()." ".$datosAseg->getNom();
        }
        $nomEmpresa = $EntityEmpresa->getNombre();

        $session->set('tipo', $tipo);
        $session->set('mat', $mat);
		$session->set('matase', $matase);
        $session->set('esAseg', $esAseg);
        $session->set('apePat', $entity->getApp());
        $session->set('apeMat', $entity->getApm());
        $session->set('nom', $entity->getNom());
		$session->set('empresaCod', $empresaId);
        $session->set('nomCom', ucwords(strtolower($nomCom)));
        $session->set('nomComAseg', ucwords(strtolower($nomComAseg)));
        $session->set('nomEmpresa', ucwords(strtolower($nomEmpresa)));
        return $this->redirect($this->generateUrl($this->rutDestino));
    }

}