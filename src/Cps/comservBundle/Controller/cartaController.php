<?php

namespace Cps\comservBundle\Controller;

use Cps\comservBundle\Entity\carta;
use Cps\comservBundle\Entity\solicitud;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Cartum controller.
 *
 * @Route("carta")
 */
class cartaController extends Controller
{
    /**
     * Lists all cartum entities.
     *
     * @Route("/", name="carta_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cartas = $em->getRepository('cpscomservBundle:carta')->findAll();

        return $this->render('carta/index.html.twig', array(
            'cartas' => $cartas,
        ));
    }

    /**
     * Creates a new carta entity.
     *
     * @Route("/new/{id}", name="carta_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request , solicitud $solicitud)
    {
        $carta = new Carta();
        $form = $this->createForm('Cps\comservBundle\Form\cartaType', $carta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $carta->setSolicitud($solicitud);
            $em->persist($carta);
            $em->flush($carta);

            return $this->redirectToRoute('carta_show', array('id' => $carta->getId()));
        }

        return $this->render('carta/new.html.twig', array(
            'carta' => $carta,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a cartum entity.
     *
     * @Route("/{id}", name="carta_show")
     * @Method("GET")
     */
    public function showAction(carta $cartum)
    {
        $deleteForm = $this->createDeleteForm($cartum);

        return $this->render('carta/show.html.twig', array(
            'cartum' => $cartum,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing cartum entity.
     *
     * @Route("/{id}/edit", name="carta_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, carta $cartum)
    {
        $deleteForm = $this->createDeleteForm($cartum);
        $editForm = $this->createForm('Cps\comservBundle\Form\cartaType', $cartum);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('carta_edit', array('id' => $cartum->getId()));
        }

        return $this->render('carta/edit.html.twig', array(
            'cartum' => $cartum,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a cartum entity.
     *
     * @Route("/{id}", name="carta_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, carta $cartum)
    {
        $form = $this->createDeleteForm($cartum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cartum);
            $em->flush($cartum);
        }

        return $this->redirectToRoute('carta_index');
    }

    /**
     * Creates a form to delete a cartum entity.
     *
     * @param carta $cartum The cartum entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(carta $cartum)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('carta_delete', array('id' => $cartum->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
