<?php

namespace Cps\comservBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * movimiento
 *
 * @ORM\Table(name="com_serv_movimiento")
 * @ORM\Entity(repositoryClass="Cps\comservBundle\Repository\movimientoRepository")
 */
class movimiento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="origen_serv", type="integer")
     */
    private $origenServ;

    /**
     * @var string
     *
     * @ORM\Column(name="destino_serv", type="integer")
     */
    private $destinoServ;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_envio", type="datetime")
     */
    private $fechaEnvio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_recibido", type="datetime", nullable=true)
     */
    private $fechaRecibido;

    /**
     * @var int
     *
     * @ORM\Column(name="usu_envio", type="integer")
     */
    private $usuEnvio;

    /**
     * @var string
     *
     * @ORM\Column(name="usu_recibido", type="integer", nullable=true)
     */
    private $usuRecibido;

    /**
     * @var string
     *
     * @ORM\Column(name="observacion", type="text", nullable=true)
     */
    private $observacion;

    /**
     * @var string
     *
     * @ORM\Column(name="carta_ref", type="string", length=255, nullable=true)
     */
    private $cartaRef;

    /**
     * @var string
     *
     * @ORM\Column(name="estado_sol", type="integer")
     */
    private $estadoSol;

    /**
     * @var int
     *
     * @ORM\Column(name="estado_reg", type="integer")
     */
    private $estadoReg;

    /**
     *@ORM\ManyToOne(targetEntity="Cps\comservBundle\Entity\solicitud", inversedBy="movimiento")
     *@ORM\JoinColumn(nullable=false)
     */
    protected $solicitud;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set origenServ
     *
     * @param integer $origenServ
     * @return movimiento
     */
    public function setOrigenServ($origenServ)
    {
        $this->origenServ = $origenServ;

        return $this;
    }

    /**
     * Get origenServ
     *
     * @return integer
     */
    public function getOrigenServ()
    {
        return $this->origenServ;
    }

    /**
     * Set destinoServ
     *
     * @param integer $destinoServ
     * @return movimiento
     */
    public function setDestinoServ($destinoServ)
    {
        $this->destinoServ = $destinoServ;

        return $this;
    }

    /**
     * Get destinoServ
     *
     * @return integer
     */
    public function getDestinoServ()
    {
        return $this->destinoServ;
    }

    /**
     * Set fechaEnvio
     *
     * @param \DateTime $fechaEnvio
     * @return movimiento
     */
    public function setFechaEnvio($fechaEnvio)
    {
        $this->fechaEnvio = $fechaEnvio;

        return $this;
    }

    /**
     * Get fechaEnvio
     *
     * @return \DateTime
     */
    public function getFechaEnvio()
    {
        return $this->fechaEnvio;
    }

    /**
     * Set fechaRecibido
     *
     * @param \DateTime $fechaRecibido
     * @return movimiento
     */
    public function setFechaRecibido($fechaRecibido)
    {
        $this->fechaRecibido = $fechaRecibido;

        return $this;
    }

    /**
     * Get fechaRecibido
     *
     * @return \DateTime
     */
    public function getFechaRecibido()
    {
        return $this->fechaRecibido;
    }

    /**
     * Set usuEnvio
     *
     * @param integer $usuEnvio
     * @return movimiento
     */
    public function setUsuEnvio($usuEnvio)
    {
        $this->usuEnvio = $usuEnvio;

        return $this;
    }

    /**
     * Get usuEnvio
     *
     * @return integer
     */
    public function getUsuEnvio()
    {
        return $this->usuEnvio;
    }

    /**
     * Set usuRecibido
     *
     * @param integer $usuRecibido
     * @return movimiento
     */
    public function setUsuRecibido($usuRecibido)
    {
        $this->usuRecibido = $usuRecibido;

        return $this;
    }

    /**
     * Get usuRecibido
     *
     * @return integer
     */
    public function getUsuRecibido()
    {
        return $this->usuRecibido;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return movimiento
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set cartaRef
     *
     * @param string $cartaRef
     * @return movimiento
     */
    public function setCartaRef($cartaRef)
    {
        $this->cartaRef = $cartaRef;

        return $this;
    }

    /**
     * Get cartaRef
     *
     * @return string
     */
    public function getCartaRef()
    {
        return $this->cartaRef;
    }

    /**
     * Set estadoSol
     *
     * @param integer $estadoSol
     * @return movimiento
     */
    public function setEstadoSol($estadoSol)
    {
        $this->estadoSol = $estadoSol;

        return $this;
    }

    /**
     * Get estadoSol
     *
     * @return integer
     */
    public function getEstadoSol()
    {
        return $this->estadoSol;
    }

    /**
     * Set estadoReg
     *
     * @param integer $estadoReg
     * @return movimiento
     */
    public function setEstadoReg($estadoReg)
    {
        $this->estadoReg = $estadoReg;

        return $this;
    }

    /**
     * Get estadoReg
     *
     * @return integer
     */
    public function getEstadoReg()
    {
        return $this->estadoReg;
    }

    /**
     * Set solicitud
     *
     * @param \Cps\comservBundle\Entity\solicitud $solicitud
     * @return movimiento
     */
    public function setSolicitud(\Cps\comservBundle\Entity\solicitud $solicitud)
    {
        $this->solicitud = $solicitud;

        return $this;
    }

    /**
     * Get solicitud
     *
     * @return \Cps\comservBundle\Entity\solicitud 
     */
    public function getSolicitud()
    {
        return $this->solicitud;
    }
}
