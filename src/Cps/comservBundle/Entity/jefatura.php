<?php

namespace Cps\comservBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * jefatura
 *
 * @ORM\Table(name="com_serv_jefatura")
 * @ORM\Entity(repositoryClass="Cps\comservBundle\Repository\jefaturaRepository")
 */
class jefatura
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="cargo", type="string", length=50)
     */
    private $cargo;

    /**
     * @var bool
     *
     * @ORM\Column(name="es_activo", type="boolean")
     */
    private $esActivo;
    /************************************************ *FORANEAS ******************************************************/

    /**
  	 *@ORM\ManyToOne(targetEntity="Cps\Administracion\AdministracionBundle\Entity\Centrocosto", inversedBy="jefatura")
  	 *@ORM\JoinColumn(nullable=true, name="centrocosto_id")
  	 */
  	protected $centrocosto;

    /**
  	 *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\carta", mappedBy="jefatura")
  	 *@ORM\JoinColumn(nullable=false)
  	 */
  	protected $carta;

    /***********************************************************************************************************************/

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return jefatura
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set cargo
     *
     * @param string $cargo
     * @return jefatura
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * Get cargo
     *
     * @return string
     */
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * Set esActivo
     *
     * @param boolean $esActivo
     * @return jefatura
     */
    public function setEsActivo($esActivo)
    {
        $this->esActivo = $esActivo;

        return $this;
    }

    /**
     * Get esActivo
     *
     * @return boolean
     */
    public function getEsActivo()
    {
        return $this->esActivo;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->carta = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set centrocosto
     *
     * @param \Cps\Administracion\AdministracionBundle\Entity\Centrocosto $centrocosto
     * @return jefatura
     */
    public function setCentrocosto(\Cps\Administracion\AdministracionBundle\Entity\Centrocosto $centrocosto = null)
    {
        $this->centrocosto = $centrocosto;

        return $this;
    }

    /**
     * Get centrocosto
     *
     * @return \Cps\Administracion\AdministracionBundle\Entity\Centrocosto 
     */
    public function getCentrocosto()
    {
        return $this->centrocosto;
    }

    /**
     * Add carta
     *
     * @param \Cps\comservBundle\Entity\carta $carta
     * @return jefatura
     */
    public function addCartum(\Cps\comservBundle\Entity\carta $carta)
    {
        $this->carta[] = $carta;

        return $this;
    }

    /**
     * Remove carta
     *
     * @param \Cps\comservBundle\Entity\carta $carta
     */
    public function removeCartum(\Cps\comservBundle\Entity\carta $carta)
    {
        $this->carta->removeElement($carta);
    }

    /**
     * Get carta
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCarta()
    {
        return $this->carta;
    }
}
