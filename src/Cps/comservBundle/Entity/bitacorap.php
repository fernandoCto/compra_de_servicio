<?php

namespace Cps\comservBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * bitacorap
 *
 * @ORM\Table(name="com_serv_bitacora_paciente")
 * @ORM\Entity(repositoryClass="Cps\comservBundle\Repository\bitacorapRepository")
 */
class bitacorap
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="matriculap", type="string", length=12)
     */
    private $matriculap;

    /**
     * @var string
     *
     * @ORM\Column(name="nombrep", type="string", length=100)
     */
    private $nombrep;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=2)
     */
    private $tipo;
    /************************************************************* FORANEA ***********************************************************************/
    /**
  	 *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\solicitud", mappedBy="bitacorap")
  	 *@ORM\JoinColumn(nullable=false)
  	 */
  	protected $solicitud;

    /**
  	 *@ORM\ManyToOne(targetEntity="Cps\comservBundle\Entity\bitacorat", inversedBy="bitacorap")
  	 *@ORM\JoinColumn(nullable=true)
  	 */
  	protected $bitacorat;

    /*******************************************************************************************************************************************/

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->solicitud = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set matriculap
     *
     * @param string $matriculap
     * @return bitacorap
     */
    public function setMatriculap($matriculap)
    {
        $this->matriculap = $matriculap;

        return $this;
    }

    /**
     * Get matriculap
     *
     * @return string
     */
    public function getMatriculap()
    {
        return $this->matriculap;
    }

    /**
     * Set nombrep
     *
     * @param string $nombrep
     * @return bitacorap
     */
    public function setNombrep($nombrep)
    {
        $this->nombrep = $nombrep;

        return $this;
    }

    /**
     * Get nombrep
     *
     * @return string
     */
    public function getNombrep()
    {
        return $this->nombrep;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return bitacorap
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Add solicitud
     *
     * @param \Cps\comservBundle\Entity\solicitud $solicitud
     * @return bitacorap
     */
    public function addSolicitud(\Cps\comservBundle\Entity\solicitud $solicitud)
    {
        $this->solicitud[] = $solicitud;

        return $this;
    }

    /**
     * Remove solicitud
     *
     * @param \Cps\comservBundle\Entity\solicitud $solicitud
     */
    public function removeSolicitud(\Cps\comservBundle\Entity\solicitud $solicitud)
    {
        $this->solicitud->removeElement($solicitud);
    }

    /**
     * Get solicitud
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSolicitud()
    {
        return $this->solicitud;
    }

    /**
     * Set bitacorat
     *
     * @param \Cps\comservBundle\Entity\bitacorat $bitacorat
     * @return bitacorap
     */
    public function setBitacorat(\Cps\comservBundle\Entity\bitacorat $bitacorat = null)
    {
        $this->bitacorat = $bitacorat;

        return $this;
    }

    /**
     * Get bitacorat
     *
     * @return \Cps\comservBundle\Entity\bitacorat
     */
    public function getBitacorat()
    {
        return $this->bitacorat;
    }
}
