<?php

namespace Cps\comservBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * verifica
 *
 * @ORM\Table(name="com_serv_verifica")
 * @ORM\Entity(repositoryClass="Cps\comservBundle\Repository\verificaRepository")
 */
class verifica
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="observacion", type="text")
     */
    private $observacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creadoel", type="datetime")
     */
    private $creadoel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modificadoel", type="datetime")
     */
    private $modificadoel;
    
     /**
      *@ORM\ManyToOne(targetEntity="Cps\comservBundle\Entity\solicitud", inversedBy="verifica")
      *@ORM\JoinColumn(nullable=false, name="solicitud_id")
      */
      protected $solicitud;
     
     /**
     *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\veridet", mappedBy="verifica")
     *@ORM\JoinColumn(nullable=false)
     */
     protected $veridet;
     /*

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     *
     * @return verifica
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return verifica
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set creadoel
     *
     * @param \DateTime $creadoel
     *
     * @return verifica
     */
    public function setCreadoel($creadoel)
    {
        $this->creadoel = $creadoel;

        return $this;
    }

    /**
     * Get creadoel
     *
     * @return \DateTime
     */
    public function getCreadoel()
    {
        return $this->creadoel;
    }

    /**
     * Set modificadoel
     *
     * @param \DateTime $modificadoel
     *
     * @return verifica
     */
    public function setModificadoel($modificadoel)
    {
        $this->modificadoel = $modificadoel;

        return $this;
    }

    /**
     * Get modificadoel
     *
     * @return \DateTime
     */
    public function getModificadoel()
    {
        return $this->modificadoel;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->veridet = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add veridet
     *
     * @param \Cps\comservBundle\Entity\veridet $veridet
     * @return verifica
     */
    public function addVeridet(\Cps\comservBundle\Entity\veridet $veridet)
    {
        $this->veridet[] = $veridet;

        return $this;
    }

    /**
     * Remove veridet
     *
     * @param \Cps\comservBundle\Entity\veridet $veridet
     */
    public function removeVeridet(\Cps\comservBundle\Entity\veridet $veridet)
    {
        $this->veridet->removeElement($veridet);
    }

    /**
     * Get veridet
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVeridet()
    {
        return $this->veridet;
    }

    /**
     * Set solicitud
     *
     * @param \Cps\comservBundle\Entity\solicitud $solicitud
     * @return verifica
     */
    public function setSolicitud(\Cps\comservBundle\Entity\solicitud $solicitud)
    {
        $this->solicitud = $solicitud;

        return $this;
    }

    /**
     * Get solicitud
     *
     * @return \Cps\comservBundle\Entity\solicitud 
     */
    public function getSolicitud()
    {
        return $this->solicitud;
    }
}
