<?php

namespace Cps\comservBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * carta
 *
 * @ORM\Table(name="com_serv_carta")
 * @ORM\Entity(repositoryClass="Cps\comservBundle\Repository\cartaRepository")
 */
class carta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=50)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="referencia", type="string", length=80)
     */
    private $referencia;

    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="text")
     */
    private $contenido;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=2)
     */
    private $tipo;

    /**
  	 * @var \DateTime
  	 *
  	 * @ORM\Column(name="creado_el", type="datetime")
  	 */
  	private $creadoEl;

  	/**
  	 * @var \DateTime
  	 *
  	 * @ORM\Column(name="modificado_el", type="datetime")
  	 */
  	private $modificadoEl;

      public function __toString() {
  		return $this->diagnostico;
  	}
    /****************************************************************** CALLBACKS ****************************************/
  	/**
  	 * @ORM\PrePersist
  	 */
  	public function setCreadoEl()
  	{
  		$this->creadoEl = new \DateTime();
  	}

  	/**
  	 * @ORM\PrePersist
  	 * @ORM\PreUpdate
  	 */
  	public function setModificadoEl()
  	{
  		$this->modificadoEl = new \DateTime();
  	}
    /*************************************************** FORANEAS ***********************************************************/
    /**
  	 *@ORM\ManyToOne(targetEntity="Cps\comservBundle\Entity\jefatura", inversedBy="carta")
  	 *@ORM\JoinColumn(nullable=false)
  	 */
  	protected $jefatura;

    /**
  	 *@ORM\ManyToOne(targetEntity="Cps\comservBundle\Entity\solicitud", inversedBy="carta")
  	 *@ORM\JoinColumn(nullable=false)
  	 */
  	protected $solicitud;


    /************************************************************************************************************************/

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     * @return carta
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set referencia
     *
     * @param string $referencia
     * @return carta
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia
     *
     * @return string
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set contenido
     *
     * @param string $contenido
     * @return carta
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return string
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return carta
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Get creadoEl
     *
     * @return \DateTime 
     */
    public function getCreadoEl()
    {
        return $this->creadoEl;
    }

    /**
     * Get modificadoEl
     *
     * @return \DateTime 
     */
    public function getModificadoEl()
    {
        return $this->modificadoEl;
    }

    /**
     * Set jefatura
     *
     * @param \Cps\comservBundle\Entity\jefatura $jefatura
     * @return carta
     */
    public function setJefatura(\Cps\comservBundle\Entity\jefatura $jefatura)
    {
        $this->jefatura = $jefatura;

        return $this;
    }

    /**
     * Get jefatura
     *
     * @return \Cps\comservBundle\Entity\jefatura 
     */
    public function getJefatura()
    {
        return $this->jefatura;
    }

    /**
     * Set solicitud
     *
     * @param \Cps\comservBundle\Entity\solicitud $solicitud
     * @return carta
     */
    public function setSolicitud(\Cps\comservBundle\Entity\solicitud $solicitud)
    {
        $this->solicitud = $solicitud;

        return $this;
    }

    /**
     * Get solicitud
     *
     * @return \Cps\comservBundle\Entity\solicitud 
     */
    public function getSolicitud()
    {
        return $this->solicitud;
    }
}
