<?php

namespace Cps\comservBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * proceso
 *
 * @ORM\Table(name="com_serv_proceso")
 * @ORM\Entity(repositoryClass="Cps\comservBundle\Repository\procesoRepository")
 */
class proceso
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=10)
     */
    private $nombre;

    /**
     *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\solicitud", mappedBy="proceso")
     *@ORM\JoinColumn(nullable=false)
     */
    protected $solicitud;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return proceso
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->solicitud = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add solicitud
     *
     * @param \Cps\comservBundle\Entity\solicitud $solicitud
     *
     * @return proceso
     */
    public function addSolicitud(\Cps\comservBundle\Entity\solicitud $solicitud)
    {
        $this->solicitud[] = $solicitud;

        return $this;
    }

    /**
     * Remove solicitud
     *
     * @param \Cps\comservBundle\Entity\solicitud $solicitud
     */
    public function removeSolicitud(\Cps\comservBundle\Entity\solicitud $solicitud)
    {
        $this->solicitud->removeElement($solicitud);
    }

    /**
     * Get solicitud
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSolicitud()
    {
        return $this->solicitud;
    }
}
