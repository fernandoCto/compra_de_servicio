<?php

namespace Cps\comservBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * bitacorat
 *
 * @ORM\Table(name="com_serv_bitacora_titular")
 * @ORM\Entity(repositoryClass="Cps\comservBundle\Repository\bitacoratRepository")
 */
class bitacorat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="matricula", type="string", length=12)
     */
    private $matricula;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /************************************************************* FORANEA ***********************************************************************/
    /**
  	 *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\bitacorap", mappedBy="bitacorat")
  	 *@ORM\JoinColumn(nullable=false)
  	 */
  	protected $bitacorap;

    /************************************************************* /FORANEA ***********************************************************************/
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set matricula
     *
     * @param string $matricula
     * @return bitacorat
     */
    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;

        return $this;
    }

    /**
     * Get matricula
     *
     * @return string
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return bitacorat
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bitacorap = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add bitacorap
     *
     * @param \Cps\comservBundle\Entity\bitacorap $bitacorap
     * @return bitacorat
     */
    public function addBitacorap(\Cps\comservBundle\Entity\bitacorap $bitacorap)
    {
        $this->bitacorap[] = $bitacorap;

        return $this;
    }

    /**
     * Remove bitacorap
     *
     * @param \Cps\comservBundle\Entity\bitacorap $bitacorap
     */
    public function removeBitacorap(\Cps\comservBundle\Entity\bitacorap $bitacorap)
    {
        $this->bitacorap->removeElement($bitacorap);
    }

    /**
     * Get bitacorap
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBitacorap()
    {
        return $this->bitacorap;
    }
}
