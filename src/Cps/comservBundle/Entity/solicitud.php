<?php
namespace Cps\comservBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * solicitud
 *
 * @ORM\Table(name="com_serv_solicitud")
 * @ORM\Entity(repositoryClass="Cps\comservBundle\Repository\solicitudRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class solicitud
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="diagnostico", type="string", length=100, nullable=false)
	 * @Assert\NotBlank())
	 */
	private $diagnostico;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="requerimiento", type="string", length=100, nullable=false)
	 * @Assert\NotBlank())
	 */
	private $requerimiento;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="pacienteId", type="string", length=12)
	 */
	private $pacienteId;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="empresa_cod", type="string", length=15)
	 */
	private $empresaCod;
	/**
	 * @var int
	 *
	 * @ORM\Column(name="medico_id", type="integer")
	 * @Assert\NotBlank())
	 */
	private $medicoId;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="origen", type="string", length=1)
	 */
	private $origen;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="estado_solicitud", type="string", length=2)
	 */
	private $estadoSolicitud;

	/**
	 * @var \DateTime
	 * @Assert\NotBlank(message="Debe ingresar la fecha."))
	 * @Assert\DateTime( message="Formato de fecha no valido.")
	 * @ORM\Column(name="fecha", type="datetime")
	 */
	private $fecha;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="creado_el", type="datetime")
	 */
	private $creadoEl;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="modificado_el", type="datetime")
	 */
	private $modificadoEl;

    public function __toString() {
		return $this->diagnostico;
	}

	/****************************************************************** CALLBACKS ********************************************************************/
	/**
	 * @ORM\PrePersist
	 */
	public function setCreadoEl()
	{
		$this->creadoEl = new \DateTime();
	}

	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function setModificadoEl()
	{
		$this->modificadoEl = new \DateTime();
	}
	/****************************************************************** FORANEA ********************************************************************/

    /**
     *@ORM\ManyToOne(targetEntity="Cps\comservBundle\Entity\proceso", inversedBy="solicitud")
     *@ORM\JoinColumn(nullable=false)
     */
    protected $proceso;

	/**
	 *@ORM\ManyToOne(targetEntity="Cps\comservBundle\Entity\user", inversedBy="solicitud")
	 *@ORM\JoinColumn(nullable=false, name="user_id")
	 */
	protected $user;

	/**
	 *@ORM\ManyToOne(targetEntity="Cps\comservBundle\Entity\tipo_sol", inversedBy="solicitud")
	 *@ORM\JoinColumn(nullable=false, name="tipo_sol_id")
	 */
	protected $tipoSol;

	/**
	 *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\proforma", mappedBy="solicitud")
	 *@ORM\JoinColumn(nullable=false)
	 */
	protected $proforma;

	/**
	 *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\factura", mappedBy="solicitud")
	 *@ORM\JoinColumn(nullable=false)
	 */
	protected $factura;

	/**
	 *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\carta", mappedBy="solicitud")
	 *@ORM\JoinColumn(nullable=false)
	 */
	protected $carta;

	/**
	 *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\verifica", mappedBy="solicitud")
	 *@ORM\JoinColumn(nullable=false)
	 */
	protected $verifica;

	/**
	 *@ORM\ManyToOne(targetEntity="Cps\comservBundle\Entity\bitacorap", inversedBy="solicitud")
	 *@ORM\JoinColumn(nullable=false, name="paciente_id")
	 */
	protected $bitacorap;

    /**
     *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\movimiento", mappedBy="solicitud")
     *@ORM\JoinColumn(nullable=false)
     */
    protected $movimiento;

    /*PENDIENTES*/
    //	/**
    //	 *@ORM\ManyToOne(targetEntity="Cps\Afiliacion\AfiliacionBundle\Entity\Empresa", inversedBy="solicitud")
    //	 *@ORM\JoinColumn(nullable=false, name="empresa_id")
    //	 */
    //	protected $empresa;

    /**************************************************************  SET AND GET************************************************************************/
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set diagnostico
	 *
	 * @param string $diagnostico
	 * @return solicitud
	 */
	public function setDiagnostico($diagnostico)
	{
		$this->diagnostico = $diagnostico;

		return $this;
	}

	/**
	 * Get diagnostico
	 *
	 * @return string
	 */
	public function getDiagnostico()
	{
		return $this->diagnostico;
	}
	/**
	 * Set pacienteId
	 *
	 * @param string $pacienteId
	 * @return solicitud
	 */
	public function setPacienteId($pacienteId)
	{
		$this->pacienteId = $pacienteId;

		return $this;
	}

	/**
	 * Get pacienteId
	 *
	 * @return string
	 */
	public function getPacienteId()
	{
		return $this->pacienteId;
	}

	/**
	 * Set tipoPaciente
	 *
	 * @param string $tipoPaciente
	 * @return solicitud
	 */
	public function setTipoPaciente($tipoPaciente)
	{
		$this->tipoPaciente = $tipoPaciente;

		return $this;
	}

	/**
	 * Get tipoPaciente
	 *
	 * @return string
	 */
	public function getTipoPaciente()
	{
		return $this->tipoPaciente;
	}

	/**
	 * Set medicoId
	 *
	 * @param integer $medicoId
	 * @return solicitud
	 */
	public function setMedicoId($medicoId)
	{
		$this->medicoId = $medicoId;

		return $this;
	}

	/**
	 * Get medicoId
	 *
	 * @return integer
	 */
	public function getMedicoId()
	{
		return $this->medicoId;
	}

	/**
	 * Set origen
	 *
	 * @param string $origen
	 * @return solicitud
	 */
	public function setOrigen($origen)
	{
		$this->origen = $origen;

		return $this;
	}

	/**
	 * Get origen
	 *
	 * @return string
	 */
	public function getOrigen()
	{
		return $this->origen;
	}

	/**
	 * Set estadoSolicitud
	 *
	 * @param string $estadoSolicitud
	 * @return solicitud
	 */
	public function setEstadoSolicitud($estadoSolicitud)
	{
		$this->estadoSolicitud = $estadoSolicitud;

		return $this;
	}

	/**
	 * Get estadoSolicitud
	 *
	 * @return string
	 */
	public function getEstadoSolicitud()
	{
		return $this->estadoSolicitud;
	}

	/**
	 * Set tipoSol
	 *
	 * @param \Cps\comservBundle\Entity\tipo_sol $tipoSol
	 * @return solicitud
	 */
	public function setTipoSol(\Cps\comservBundle\Entity\tipo_sol $tipoSol)
	{
		$this->tipoSol = $tipoSol;

		return $this;
	}

	/**
	 * Get tipoSol
	 *
	 * @return \Cps\comservBundle\Entity\tipo_sol
	 */
	public function getTipoSol()
	{
		return $this->tipoSol;
	}

	/**
	 * Set requerimiento
	 *
	 * @param string $requerimiento
	 * @return solicitud
	 */
	public function setRequerimiento($requerimiento)
	{
		$this->requerimiento = $requerimiento;

		return $this;
	}

	/**
	 * Get requerimiento
	 *
	 * @return string
	 */
	public function getRequerimiento()
	{
		return $this->requerimiento;
	}

	/**
	 * Set fecha
	 *
	 * @param \DateTime $fecha
	 * @return solicitud
	 */
	public function setFecha($fecha)
	{
		$this->fecha = $fecha;

		return $this;
	}

	/**
	 * Get fecha
	 *
	 * @return \DateTime
	 */
	public function getFecha()
	{
		return $this->fecha;
	}

	/**
	 * Set creadoEl
	 *
	 * @param \DateTime $creadoEl
	 * @return solicitud
	 */

	/**
	 * Get creadoEl
	 *
	 * @return \DateTime
	 */
	public function getCreadoEl()
	{
		return $this->creadoEl;
	}

	/**
	 * Set modificadoEl
	 *
	 * @param \DateTime $modificadoEl
	 * @return solicitud
	 */

	/**
	 * Get modificadoEl
	 *
	 * @return \DateTime
	 */
	public function getModificadoEl()
	{
		return $this->modificadoEl;
	}
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->proforma = new \Doctrine\Common\Collections\ArrayCollection();
        $this->factura = new \Doctrine\Common\Collections\ArrayCollection();
        $this->verifica = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Add proforma
     *
     * @param \Cps\comservBundle\Entity\proforma $proforma
     * @return solicitud
     */
    public function addProforma(\Cps\comservBundle\Entity\proforma $proforma)
    {
        $this->proforma[] = $proforma;

        return $this;
    }

    /**
     * Remove proforma
     *
     * @param \Cps\comservBundle\Entity\proforma $proforma
     */
    public function removeProforma(\Cps\comservBundle\Entity\proforma $proforma)
    {
        $this->proforma->removeElement($proforma);
    }

    /**
     * Get proforma
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProforma()
    {
        return $this->proforma;
    }

    /**
     * Add factura
     *
     * @param \Cps\comservBundle\Entity\factura $factura
     * @return solicitud
     */
    public function addFactura(\Cps\comservBundle\Entity\factura $factura)
    {
        $this->factura[] = $factura;

        return $this;
    }

    /**
     * Remove factura
     *
     * @param \Cps\comservBundle\Entity\factura $factura
     */
    public function removeFactura(\Cps\comservBundle\Entity\factura $factura)
    {
        $this->factura->removeElement($factura);
    }

    /**
     * Get factura
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFactura()
    {
        return $this->factura;
    }

    /**
     * Add verifica
     *
     * @param \Cps\comservBundle\Entity\verifica $verifica
     * @return solicitud
     */
    public function addVerifica(\Cps\comservBundle\Entity\verifica $verifica)
    {
        $this->verifica[] = $verifica;

        return $this;
    }

    /**
     * Remove verifica
     *
     * @param \Cps\comservBundle\Entity\verifica $verifica
     */
    public function removeVerifica(\Cps\comservBundle\Entity\verifica $verifica)
    {
        $this->verifica->removeElement($verifica);
    }

    /**
     * Get verifica
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVerifica()
    {
        return $this->verifica;
    }

    /**
     * Set user
     *
     * @param \Cps\comservBundle\Entity\user $user
     * @return solicitud
     */
    public function setUser(\Cps\comservBundle\Entity\user $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Cps\comservBundle\Entity\user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set empresaCod
     *
     * @param string $empresaCod
     * @return solicitud
     */
    public function setEmpresaCod($empresaCod)
    {
        $this->empresaCod = $empresaCod;

        return $this;
    }

    /**
     * Get empresaCod
     *
     * @return string
     */
    public function getEmpresaCod()
    {
        return $this->empresaCod;
    }

    /**
     * Set empresa
     *
     * @param \Cps\Afiliacion\AfiliacionBundle\Entity\Empresa $empresa
     * @return solicitud
     */
    public function setEmpresa(\Cps\Afiliacion\AfiliacionBundle\Entity\Empresa $empresa)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return \Cps\Afiliacion\AfiliacionBundle\Entity\Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set bitacorap
     *
     * @param \Cps\comservBundle\Entity\bitacorap $bitacorap
     * @return solicitud
     */
    public function setBitacorap(\Cps\comservBundle\Entity\bitacorap $bitacorap)
    {
        $this->bitacorap = $bitacorap;

        return $this;
    }

    /**
     * Get bitacorap
     *
     * @return \Cps\comservBundle\Entity\bitacorap
     */
    public function getBitacorap()
    {
        return $this->bitacorap;
    }

    /**
     * Add carta
     *
     * @param \Cps\comservBundle\Entity\carta $carta
     * @return solicitud
     */
    public function addCartum(\Cps\comservBundle\Entity\carta $carta)
    {
        $this->carta[] = $carta;

        return $this;
    }

    /**
     * Remove carta
     *
     * @param \Cps\comservBundle\Entity\carta $carta
     */
    public function removeCartum(\Cps\comservBundle\Entity\carta $carta)
    {
        $this->carta->removeElement($carta);
    }

    /**
     * Get carta
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCarta()
    {
        return $this->carta;
    }

    /**
     * Add movimiento
     *
     * @param \Cps\comservBundle\Entity\movimiento $movimiento
     * @return solicitud
     */
    public function addMovimiento(\Cps\comservBundle\Entity\movimiento $movimiento)
    {
        $this->movimiento[] = $movimiento;

        return $this;
    }

    /**
     * Remove movimiento
     *
     * @param \Cps\comservBundle\Entity\movimiento $movimiento
     */
    public function removeMovimiento(\Cps\comservBundle\Entity\movimiento $movimiento)
    {
        $this->movimiento->removeElement($movimiento);
    }

    /**
     * Get movimiento
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMovimiento()
    {
        return $this->movimiento;
    }

    /**
     * Set proceso
     *
     * @param \Cps\comservBundle\Entity\proceso $proceso
     *
     * @return solicitud
     */
    public function setProceso(\Cps\comservBundle\Entity\proceso $proceso = null)
    {
        $this->proceso = $proceso;

        return $this;
    }

    /**
     * Get proceso
     *
     * @return \Cps\comservBundle\Entity\proceso
     */
    public function getProceso()
    {
        return $this->proceso;
    }
}
