<?php

namespace Cps\comservBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * proveedor
 *
 * @ORM\Table(name="com_serv_proveedor")
 * @ORM\Entity(repositoryClass="Cps\comservBundle\Repository\proveedorRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class proveedor
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, unique=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=100)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=15)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="celular", type="string", length=15)
     */
    private $celular;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creadoel", type="datetime")
     */
    private $creadoel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modificadoel", type="datetime")
     */
    private $modificadoel;

    // public function __construct() {
    //     $this->facturap = new ArrayCollection();
    //     $this->proformap = new ArrayCollection();
    // }
	/****************************************************************** CALLBACKS ********************************************************************/
	/**
	 * @ORM\PrePersist
	 */
	public function setCreadoEl()
	{
		$this->creadoel = new \DateTime();
	}

	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function setModificadoEl()
	{
		$this->modificadoel = new \DateTime();
	}
    /////////FORANEAS
    
    /**
     *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\factura", mappedBy="proveedor")
     *@ORM\JoinColumn(nullable=false)
     */
     protected $factura;
    
    /**
     *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\proforma", mappedBy="proveedor")
     *@ORM\JoinColumn(nullable=false)
     */
     protected $proforma;
    
     public function __toString() {
		return $this->nombre;
	}
    ////////////////////////// GET y SET

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->factura = new \Doctrine\Common\Collections\ArrayCollection();
        $this->proforma = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return proveedor
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return proveedor
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return proveedor
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set celular
     *
     * @param string $celular
     * @return proveedor
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string 
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Get creadoel
     *
     * @return \DateTime 
     */
    public function getCreadoel()
    {
        return $this->creadoel;
    }

    /**
     * Get modificadoel
     *
     * @return \DateTime 
     */
    public function getModificadoel()
    {
        return $this->modificadoel;
    }

    /**
     * Add factura
     *
     * @param \Cps\comservBundle\Entity\factura $factura
     * @return proveedor
     */
    public function addFactura(\Cps\comservBundle\Entity\factura $factura)
    {
        $this->factura[] = $factura;

        return $this;
    }

    /**
     * Remove factura
     *
     * @param \Cps\comservBundle\Entity\factura $factura
     */
    public function removeFactura(\Cps\comservBundle\Entity\factura $factura)
    {
        $this->factura->removeElement($factura);
    }

    /**
     * Get factura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFactura()
    {
        return $this->factura;
    }

    /**
     * Add proforma
     *
     * @param \Cps\comservBundle\Entity\proforma $proforma
     * @return proveedor
     */
    public function addProforma(\Cps\comservBundle\Entity\proforma $proforma)
    {
        $this->proforma[] = $proforma;

        return $this;
    }

    /**
     * Remove proforma
     *
     * @param \Cps\comservBundle\Entity\proforma $proforma
     */
    public function removeProforma(\Cps\comservBundle\Entity\proforma $proforma)
    {
        $this->proforma->removeElement($proforma);
    }

    /**
     * Get proforma
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProforma()
    {
        return $this->proforma;
    }
}
