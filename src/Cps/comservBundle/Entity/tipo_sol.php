<?php

namespace Cps\comservBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * tipo_sol
 *
 * @ORM\Table(name="com_serv_tipo_sol")
 * @ORM\Entity(repositoryClass="Cps\comservBundle\Repository\tipo_solRepository")
 */
class tipo_sol
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=25)
     */
    private $nombre;

    public function __toString() {
        return $this->nombre;
    }


    /*---------------------------------------------------------- FORANEAS ---------------------------------------------------------------------------------------*/
    /**
     *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\solicitud", mappedBy="tipoSol")
     *@ORM\JoinColumn(nullable=false)
     */
    protected $solicitud;

    /*************************************************************************************************************************************************************/
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return tipo_sol
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->solicitud = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add solicitud
     *
     * @param \Cps\comservBundle\Entity\solicitud $solicitud
     * @return tipo_sol
     */
    public function addSolicitud(\Cps\comservBundle\Entity\solicitud $solicitud)
    {
        $this->solicitud[] = $solicitud;

        return $this;
    }

    /**
     * Remove solicitud
     *
     * @param \Cps\comservBundle\Entity\solicitud $solicitud
     */
    public function removeSolicitud(\Cps\comservBundle\Entity\solicitud $solicitud)
    {
        $this->solicitud->removeElement($solicitud);
    }

    /**
     * Get solicitud
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSolicitud()
    {
        return $this->solicitud;
    }
}
