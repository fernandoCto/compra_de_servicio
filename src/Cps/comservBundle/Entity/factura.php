<?php

namespace Cps\comservBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * factura
 *
 * @ORM\Table(name="com_serv_factura")
 * @ORM\Entity(repositoryClass="Cps\comservBundle\Repository\facturaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class factura
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="integer")
     */
    private $numero;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creadoel", type="datetime")
     */
    private $creadoel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modificadoel", type="datetime")
     */
    private $modificadoel;

	/****************************************************************** CALLBACKS ********************************************************************/
	/**
	 * @ORM\PrePersist
	 */
	public function setCreadoEl()
	{
		$this->creadoel = new \DateTime();
	}

	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function setModificadoEl()
	{
        $this->modificadoel = new \DateTime();
    }    

/////////FORANEAS
    /**
     *
     *@ORM\ManyToOne(targetEntity="Cps\comservBundle\Entity\solicitud", inversedBy="factura")
     *@ORM\JoinColumn(nullable=true, name="solicitud_id")
     */
     protected $solicitud;
     
    /**
     * 
     *@ORM\ManyToOne(targetEntity="Cps\comservBundle\Entity\proveedor", inversedBy="factura")
     *@ORM\JoinColumn(nullable=false, name="proveedor_id")
     */

    protected $proveedor;
     

////////GET Y SET

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return factura
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     * @return factura
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return factura
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Get creadoel
     *
     * @return \DateTime 
     */
    public function getCreadoel()
    {
        return $this->creadoel;
    }

    /**
     * Get modificadoel
     *
     * @return \DateTime 
     */
    public function getModificadoel()
    {
        return $this->modificadoel;
    }

    /**
     * Set solicitud
     *
     * @param \Cps\comservBundle\Entity\solicitud $solicitud
     * @return factura
     */
    public function setSolicitud(\Cps\comservBundle\Entity\solicitud $solicitud)
    {
        $this->solicitud = $solicitud;

        return $this;
    }

    /**
     * Get solicitud
     *
     * @return \Cps\comservBundle\Entity\solicitud 
     */
    public function getSolicitud()
    {
        return $this->solicitud;
    }

    /**
     * Set proveedor
     *
     * @param \Cps\comservBundle\Entity\proveedor $proveedor
     * @return factura
     */
    public function setProveedor(\Cps\comservBundle\Entity\proveedor $proveedor)
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor
     *
     * @return \Cps\comservBundle\Entity\proveedor 
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }
}
