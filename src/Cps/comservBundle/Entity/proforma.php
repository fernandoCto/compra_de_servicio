<?php

namespace Cps\comservBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * proforma
 *
 * @ORM\Table(name="com_serv_proforma")
 * @ORM\Entity(repositoryClass="Cps\comservBundle\Repository\proformaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class proforma
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=150)
     */
    private $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;

    /**
     * @var string
     *
     * @ORM\Column(name="observacion", type="text")
     */
    private $observacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creadoel", type="datetime")
     */
    private $creadoel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modificadoel", type="datetime")
     */
    private $modificadoel;

    /****************************************************************** CALLBACKS ********************************************************************/
    /**
     * @ORM\PrePersist
     */
    public function setCreadoEl()
    {
        $this->creadoel = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setModificadoEl()
    {
        $this->modificadoel = new \DateTime();
    }

    /**
     *
     *@ORM\ManyToOne(targetEntity="Cps\comservBundle\Entity\solicitud", inversedBy="proforma")
     *@ORM\JoinColumn(nullable=false, name="solicitud_id")
     */
    protected $solicitud;

    /**
     *@ORM\ManyToOne(targetEntity="Cps\comservBundle\Entity\proveedor", inversedBy="proforma")
     *@ORM\JoinColumn(nullable=false, name="proveedor_id")
     */
     protected $proveedor;
     

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return proforma
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return proforma
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return proforma
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return proforma
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Get creadoel
     *
     * @return \DateTime 
     */
    public function getCreadoel()
    {
        return $this->creadoel;
    }

    /**
     * Get modificadoel
     *
     * @return \DateTime 
     */
    public function getModificadoel()
    {
        return $this->modificadoel;
    }

    /**
     * Set solicitud
     *
     * @param \Cps\comservBundle\Entity\solicitud $solicitud
     * @return proforma
     */
    public function setSolicitud(\Cps\comservBundle\Entity\solicitud $solicitud)
    {
        $this->solicitud = $solicitud;

        return $this;
    }

    /**
     * Get solicitud
     *
     * @return \Cps\comservBundle\Entity\solicitud 
     */
    public function getSolicitud()
    {
        return $this->solicitud;
    }

    /**
     * Set proveedor
     *
     * @param \Cps\comservBundle\Entity\proveedor $proveedor
     * @return proforma
     */
    public function setProveedor(\Cps\comservBundle\Entity\proveedor $proveedor)
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor
     *
     * @return \Cps\comservBundle\Entity\proveedor 
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }
}
