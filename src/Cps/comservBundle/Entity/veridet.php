<?php

namespace Cps\comservBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * veridet
 *
 * @ORM\Table(name="com_serv_veridet")
 * @ORM\Entity(repositoryClass="Cps\comservBundle\Repository\veridetRepository")
 */
class veridet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="integer")
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="requisito", type="string", length=100)
     */
    private $requisito;

    /**
     * @var string
     *
     * @ORM\Column(name="aplica", type="string", length=1)
     */
    private $aplica;

    /**
     * @var string
     *
     * @ORM\Column(name="observacion", type="string", length=150)
     */
    private $observacion;


    /**
     *@ORM\ManyToOne(targetEntity="Cps\comservBundle\Entity\verifica", inversedBy="veridet")
     *@ORM\JoinColumn(nullable=false, name="verifica_id")
     */
     protected $verifica;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return veridet
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return int
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set requisito
     *
     * @param string $requisito
     *
     * @return veridet
     */
    public function setRequisito($requisito)
    {
        $this->requisito = $requisito;

        return $this;
    }

    /**
     * Get requisito
     *
     * @return string
     */
    public function getRequisito()
    {
        return $this->requisito;
    }

    /**
     * Set aplica
     *
     * @param string $aplica
     *
     * @return veridet
     */
    public function setAplica($aplica)
    {
        $this->aplica = $aplica;

        return $this;
    }

    /**
     * Get aplica
     *
     * @return string
     */
    public function getAplica()
    {
        return $this->aplica;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     *
     * @return veridet
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set verifica
     *
     * @param \Cps\comservBundle\Entity\verifica $verifica
     * @return veridet
     */
    public function setVerifica(\Cps\comservBundle\Entity\verifica $verifica)
    {
        $this->verifica = $verifica;

        return $this;
    }

    /**
     * Get verifica
     *
     * @return \Cps\comservBundle\Entity\verifica 
     */
    public function getVerifica()
    {
        return $this->verifica;
    }
}
