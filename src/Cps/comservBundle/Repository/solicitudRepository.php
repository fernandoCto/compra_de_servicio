<?php

namespace Cps\comservBundle\Repository;

use Doctrine\ORM\EntityRepository;
use DoctrineExtensions\Query\Mysql\Month;


/**
 * solicitudRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class solicitudRepository extends EntityRepository
{
    public function BuscarSolicitudesPorPaciente($usuario, $BuscarNombre, $est){
        $em=$this->getEntityManager();
        $query = $em->createQuery("SELECT s
                                     FROM cpscomservBundle:solicitud s
                                     JOIN s.bitacorap b
                                     JOIN s.proceso p
                                     WHERE s.proceso = :est and s.estadoSolicitud = 'AC' and s.user = :user and b.nombrep LIKE '%$BuscarNombre%' ORDER BY s.fecha DESC")
                                     ->setParameter('user',$usuario)
                                     ->setParameter('est',$est);
        $result = $query->getResult();
        return $result;
    }

    public function reporteOrigen(){
        $em=$this->getEntityManager();
        $query = $em->createQuery("SELECT count(s.origen) as value, b.nombre as name
                                     FROM cpscomservBundle:solicitud s
                                     JOIN s.user u
                                     JOIN u.Centrocosto c
                                     JOIN c.ubicacion b
                                     GROUP BY s.origen");
        $result = $query->getResult();
        return $result;
    }
    public function reporteProceso(){
        $em=$this->getEntityManager();
        $query = $em->createQuery("SELECT count(p.nombre) as value, p.nombre as name
                                     FROM cpscomservBundle:solicitud s
                                     JOIN s.proceso p
                                     GROUP BY p.nombre ORDER  BY p.id ");
        $result = $query->getResult();
        return $result;
    }

}
//SELECT YEAR(fecha) Año,MONTH(fecha) Mes,COUNT(*) Registros
//FROM COM_SERV_SOLICITUD
//WHERE fecha
//GROUP BY YEAR(fecha),MONTH(fecha);
