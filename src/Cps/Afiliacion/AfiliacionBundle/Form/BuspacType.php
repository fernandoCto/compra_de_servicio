<?php

namespace Cps\Afiliacion\AfiliacionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BuspacType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('filtro', null, array('required' => false))
        ;
    }

    public function getName(){
        return 'cps_afiliacionbundle_buspactype';
    }
}
