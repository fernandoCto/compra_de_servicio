<?php

namespace Cps\Afiliacion\AfiliacionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="afi_afi_estVigencia")
 * @ORM\Entity()
 */
class Estvigencia{

    public function __construct(){
        $this->empresas = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $sigla;
    
// === Foraneas ======================================================== //    

    /**
    * @ORM\OneToMany(targetEntity="Empresa", mappedBy="estVigencia")
    */
    protected $empresas;    
    
// === Funciones Auxiliares ============================================ //

    public function __toString(){
        return $this->nombre;
    }        
    
// === Getter ========================================================= //

    /**
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return string 
     */
    public function getNombre(){
        return $this->nombre;
    }

    /**
     * @return string 
     */
    public function getSigla(){
        return $this->sigla;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEmpresas(){
        return $this->empresas;
    }
}
