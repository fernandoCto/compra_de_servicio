<?php

namespace Cps\Afiliacion\AfiliacionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="afi_afi_tipoRelacion")
 * @ORM\Entity()
 */
class Tiporelacion{

    public function __construct(){
        $this->bnefs    = new \Doctrine\Common\Collections\ArrayCollection();
    }
 
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=12)
     */
    private $nombre;
    
// === Foraneas ======================================================== //

   /**
    * @ORM\OneToMany(targetEntity="Bnef", mappedBy="tipoRelacion")
    */
   protected $bnefs;    

// === Funciones Auxiliares ============================================ //
 
    public function __toString(){
        return $this->getNombre();
    }    

// === Gets =========================================================== //    

    /**
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return string 
     */
    public function getNombre(){
        return $this->nombre;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBnefs(){
        return $this->bnefs;
    }
}
