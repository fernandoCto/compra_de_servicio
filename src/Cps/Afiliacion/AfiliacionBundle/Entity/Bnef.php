<?php

namespace Cps\Afiliacion\AfiliacionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="afi_afi_bnef",
              indexes={ @ORM\Index(name="app_bnef_idx",  columns={"app"}),
                        @ORM\Index(name="apm_bnef_idx",  columns={"apm"}),
                        @ORM\Index(name="nom_bnef_idx",  columns={"nom"}),
                        @ORM\Index(name="asegurado_idx", columns={"aseguradoId"})
                      }
             )
 * @ORM\Entity()
 */
class Bnef{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=12, unique=true)
     */
    private $cod;
    
    /**
     * @ORM\Column(name="aseguradoId", type="string", length=12)
     */
    private $aseguradoId;
    
    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $app;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $apm;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fch_ing;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fch_ret;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $amp;

// === Foraneas ======================================================== //

   /**
    * @ORM\ManyToOne(targetEntity="Tiporelacion", inversedBy="bnefs")
    * @ORM\JoinColumn(name="tipoRelacion_id", nullable=false)
    */
   protected $tipoRelacion;
   
// === Funciones Auxiliares ============================================ //

    public function getNomCompleto(){
        return $this->getApp(). ' '. $this->getApm(). ' '. $this->getNom();
    }
    
    public function getApp(){
        return ucwords(strtolower($this->app));
    }

    public function getApm(){
        return ucwords(strtolower($this->apm));
    }

    public function getNom(){
        return ucwords(strtolower($this->nom));
    }
    
// === Gets ======================================================== //        

    /**
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return string 
     */
    public function getCod(){
        return $this->cod;
    }

    /**
     * @return string 
     */
    public function getAseguradoId(){
        return $this->aseguradoId;
    }

    /**
     * @return \DateTime 
     */
    public function getFchIng(){
        return $this->fch_ing;
    }

    /**
     * @return \DateTime 
     */
    public function getFchRet(){
        return $this->fch_ret;
    }

    /**
     * @return string 
     */
    public function getAmp(){
        return $this->amp;
    }

    /**
     * @return \Cps\Afiliacion\AfiliacionBundle\Entity\Tiporelacion 
     */
    public function getTipoRelacion(){
        return $this->tipoRelacion;
    }
}
