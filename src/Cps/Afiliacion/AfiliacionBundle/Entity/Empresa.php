<?php

namespace Cps\Afiliacion\AfiliacionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="afi_afi_empresa")
 * @ORM\Entity(repositoryClass="Cps\Afiliacion\AfiliacionBundle\Entity\EmpresaRepository")
 */
class Empresa{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=12, unique=true)
     */
    private $cod;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $sigla;
    
    /**
     * @ORM\Column(name="codContable", type="string", length=13, nullable=true)
     */
    private $codContable;

    /**
     * @ORM\Column(name="ingresoEl", type="date")
     */
    private $ingresoEl;
    
    /**
     * @ORM\Column(name="retiroEl", type="date", nullable=true)
     */
    private $retiroEl;
    
    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $direccion;
    
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $telf;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $repr1;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $telf1;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $repr2;
    
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $telf2;
    
// === Foraneas ======================================================== //

    /**
     * @ORM\ManyToOne(targetEntity="Cps\Administracion\AdministracionBundle\Entity\Gerenciaadm", inversedBy="pagoEmpresas")
     * @ORM\JoinColumn(name="gerenciaAdmPago_id", nullable=false)
     */
    private $gerenciaAdmPago;
    
    /**
     * @ORM\ManyToOne(targetEntity="Claempresa", inversedBy="empresas")
     * @ORM\JoinColumn(name="claEmpresa_id", nullable=false)
     */
    private $claEmpresa;

    /**
     * @ORM\ManyToOne(targetEntity="Estvigencia", inversedBy="empresas")
     * @ORM\JoinColumn(name="estVigencia_id", nullable=false)
     */
    private $estVigencia;

    /**
	 *@ORM\OneToMany(targetEntity="Cps\comservBundle\Entity\solicitud", mappedBy="empresas")
	 *@ORM\JoinColumn(nullable=false)
	 */
	protected $solicitud;

// === Funciones Auxiliares ============================================ //

    public function setId($id){
        $this->id = $id;
    
        return $this;
    }
    
    public function __toString(){
        return $this->getNombre();
    }
    
    public function getNombre(){
        return ucwords(strtolower($this->nombre));
    }
    
// === Getter ========================================================== //    

    /**
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return string 
     */
    public function getCod(){
        return $this->cod;
    }

    /**
     * @return string 
     */
    public function getSigla(){
        return $this->sigla;
    }

    /**
     * @return string 
     */
    public function getCodContable(){
        return $this->codContable;
    }

    /**
     * @return \DateTime 
     */
    public function getIngresoEl(){
        return $this->ingresoEl;
    }

    /**
     * @return \DateTime 
     */
    public function getRetiroEl(){
        return $this->retiroEl;
    }

    /**
     * @return string 
     */
    public function getDireccion(){
        return $this->direccion;
    }

    /**
     * @return string 
     */
    public function getTelf(){
        return $this->telf;
    }

    /**
     * @return string 
     */
    public function getRepr1(){
        return $this->repr1;
    }

    /**
     * @return string 
     */
    public function getTelf1(){
        return $this->telf1;
    }

    /**
     * @return string 
     */
    public function getRepr2(){
        return $this->repr2;
    }

    /**
     * @return string 
     */
    public function getTelf2(){
        return $this->telf2;
    }

    /**
     * @return \Cps\Administracion\AdministracionBundle\Entity\Gerenciaadm 
     */
    public function getGerenciaAdmPago(){
        return $this->gerenciaAdmPago;
    }

    /**
     * @return \Cps\Afiliacion\AfiliacionBundle\Entity\Claempresa 
     */
    public function getClaEmpresa(){
        return $this->claEmpresa;
    }

    /**
     * @return \Cps\Afiliacion\AfiliacionBundle\Entity\Estvigencia 
     */
    public function getEstVigencia(){
        return $this->estVigencia;
    }
}
