<?php

namespace Cps\Afiliacion\AfiliacionBundle\Entity;

use Doctrine\ORM\EntityRepository;
use General\ComunBundle\Libreria\Filtrar;

class EmpresaRepository extends EntityRepository{

   public function cantReg($repo){
        $em = $this->getEntityManager();
        $consul = $em->createQuery('SELECT count(z.id) FROM '.$repo.' z');
        return $consul->getSingleScalarResult();
    }

    public function findTodos($repo, $filtro){                                     
        $em = $this->getEntityManager();  
        $consulta = $em->createQueryBuilder()
                        ->select('z')
                        ->from($repo, 'z')
                        ->leftJoin('z.claEmpresa', 'c')
                        ->leftJoin('z.estVigencia', 'e')
                        ->leftJoin('z.gerenciaAdmPago', 'g')
                        ;
        if ($filtro != " "){
            $consulta->where('1 = 1');
            $arreglo = explode(";", $filtro);
            $tam = sizeof($arreglo);
            $funFil = new Filtrar();
            for($i = 0; $i < $tam; ++$i){
                if ($arreglo[$i+1] == 'N' or $arreglo[$i+1] == 'NN'){
                    $consulta->andWhere('z.'.$arreglo[$i].' '.$funFil->demeValido($arreglo[$i+1]));
                    $i++;
                }else{
                    if ($arreglo[$i+1] == 'L' or $arreglo[$i+1] == 'NL') $arreglo[$i+2] = '%'.$arreglo[$i+2].'%';
                    $consulta->andWhere('z.'.$arreglo[$i].' '.$funFil->demeValido($arreglo[$i+1]).' :'.$arreglo[$i]);
                    $consulta->setParameter($arreglo[$i], $arreglo[$i+2]);
                    $i = $i + 2;
                }        
            } 
        }
        return $consulta->getQuery();
    }
    
     public function findActivasCuadro(){
        $em = $this->getEntityManager();
        $consul = $em->createQuery('SELECT z.id, z.nroPatronal, z.nombre, 0 as aseg, 0 as bnefSinAmp, 0 as bnefAmp FROM CpsAfiliacionBundle:Empresa z ORDER BY z.nombre');
        return $consul->getResult();
    }
    
     public function findActivas(){
        $em = $this->getEntityManager();
        $consul = $em->createQuery('SELECT z FROM CpsAfiliacionBundle:Empresa z');
        return $consul->getResult();
    }
    public function nombreEmpresa($empresaId){
        $em = $this->getEntityManager();
        $consul = $em->createQuery('SELECT z FROM CpsAfiliacionBundle:Empresa z
                                        WHERE z.cod=:cod')
                     ->setParameter('cod', $empresaId);
        $resp = $consul->getResult();
        return $resp;
    }
}
