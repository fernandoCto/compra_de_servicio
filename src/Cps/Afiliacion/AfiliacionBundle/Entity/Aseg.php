<?php

namespace Cps\Afiliacion\AfiliacionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="afi_afi_aseg",
              indexes={ @ORM\Index(name="app_aseg_idx", columns={"app"}),
                        @ORM\Index(name="apm_aseg_idx", columns={"apm"}),
                        @ORM\Index(name="nom_aseg_idx", columns={"nom"}),
                        @ORM\Index(name="empresa_idx",  columns={"empresaId"})
                      }
             )
 * @ORM\Entity(repositoryClass="Cps\Afiliacion\AfiliacionBundle\Entity\AsegRepository")
 */
class Aseg{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=12, unique=true)
     */
    private $cod;
    
    /**
     * @ORM\Column(name="empresaId", type="string", length=12)
     */
    private $empresaId;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fch_afi;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fch_ing;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fch_ret;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fch_baj;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fch_alt;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $app;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $apm;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $cedula;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $dir;

// === Funciones Auxiliares ============================================ //

    public function getNomCompleto(){
        return $this->getApp(). ' '. $this->getApm(). ' '. $this->getNom();
    }
    
    public function getApp(){
        return ucwords(strtolower($this->app));
    }

    public function getApm(){
        return ucwords(strtolower($this->apm));
    }

    public function getNom(){
        return ucwords(strtolower($this->nom));
    }
               
// === Gets ========================================================= //        

    /**
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return string 
     */
    public function getCod(){
        return $this->cod;
    }

    /**
     * @return string 
     */
    public function getEmpresaId(){
        return $this->empresaId;
    }

    /**
     * @return \DateTime 
     */
    public function getFchAfi(){
        return $this->fch_afi;
    }

    /**
     * @return \DateTime 
     */
    public function getFchIng(){
        return $this->fch_ing;
    }

    /**
     * @return \DateTime 
     */
    public function getFchRet(){
        return $this->fch_ret;
    }

    /**
     * @return \DateTime 
     */
    public function getFchBaj(){
        return $this->fch_baj;
    }

    /**
     * @return \DateTime 
     */
    public function getFchAlt(){
        return $this->fch_alt;
    }

    /**
     * @return string 
     */
    public function getCedula(){
        return $this->cedula;
    }

    /**
     * @return string 
     */
    public function getDir(){
        return $this->dir;
    }
}
