<?php

namespace Cps\Afiliacion\AfiliacionBundle\Entity;

use Doctrine\ORM\EntityRepository;
use General\ComunBundle\Libreria\ConexionBD;

class AsegRepository extends EntityRepository{

    public function findAsegBnefXMat($mat){
        $em = $this->getEntityManager();
        $mat = strtoupper($mat);
        $consul = $em->createQuery("SELECT a.cod, a.app, a.apm, a.nom FROM CpsAfiliacionBundle:Aseg a 
                                           WHERE a.cod LIKE '%".$mat."%'
                                    UNION 
                                    SELECT b.cod, b.app, b.apm, b.nom FROM CpsAfiliacionBundle:Bnef b
                                           WHERE b.cod LIKE '%".$mat."%'
                                    "
                                  );    
        return $consul->getResult();        
    }
    
    public function findAsegBnefXNom($app, $apm, $nom){           
        $em = $this->getEntityManager();
        $w1 = "WHERE 1=1";
        $w2 = "WHERE a.cod=b.aseguradoId";
        if ($app != ''){
            $app = strtoupper($app);
            $w1 .= " AND a.app LIKE '%".$app."%'";
            $w2 .= " AND b.app LIKE '%".$app."%'";
        }
        if ($apm != ''){
            $apm = strtoupper($apm);
            $w1 .= " AND a.apm LIKE '%".$apm."%'";
            $w2 .= " AND b.apm LIKE '%".$apm."%'";
        }
        if ($nom != ''){
            $nom = strtoupper($nom);
            $w1 .= "AND a.nom LIKE '%".$nom."%'";
            $w2 .= "AND b.nom LIKE '%".$nom."%'";
        }
            
        $consul1 = $em->createQuery("SELECT 1 as esAseg, a.cod, a.app, a.apm, a.nom, a.empresaId FROM CpsAfiliacionBundle:Aseg a ".$w1." ORDER BY a.app, a.apm, a.nom")
                    ->getResult();
        $consul2 = $em->createQuery("SELECT 0 as esAseg, b.cod, b.app, b.apm, b.nom, a.empresaId, a.cod AS aseguradId FROM CpsAfiliacionBundle:Aseg a JOIN CpsAfiliacionBundle:Bnef b ".$w2." ORDER BY b.app, b.apm, b.nom")
                    ->getResult();
        return array('aseg' => $consul1, 'bnef' => $consul2);
    }
    
    public function findPacXFiltro($filtro){           
        $em = $this->getEntityManager();
        $w1 = "WHERE 1=1";
        if ($filtro != ''){
            $filtro = strtoupper($filtro);
            $agujas = explode(" ", $filtro);
            $cant = count($agujas);
            for ($i=0; $i<$cant; $i++){
                $w1 .= " AND (z.cod LIKE '%".$agujas[$i]."%'".
                       "   OR z.nom LIKE '%".$agujas[$i]."%'".
                       "   OR z.app LIKE '%".$agujas[$i]."%'".
                       "   OR z.apm LIKE '%".$agujas[$i]."%')"
                       ;
            }
        }

        $consul1 = $em->createQuery("SELECT z.cod, z.app, z.apm, z.nom FROM CpsAfiliacionBundle:Aseg z ".$w1." ORDER BY z.app, z.apm, z.nom")
                    ->getResult();
        $consul2 = $em->createQuery("SELECT z.cod, z.app, z.apm, z.nom, z.aseguradoId FROM CpsAfiliacionBundle:Bnef z ".$w1." ORDER BY z.app, z.apm, z.nom")
                    ->getResult();
        return array('aseg' => $consul1, 'bnef' => $consul2);
    }
    
    
    public function findPac($tip, $mat){
        $em = $this->getEntityManager();
        if ($tip == 'A')
            $consul = $em->createQuery("SELECT a.cod, a.app, a.apm, a.nom FROM CpsAfiliacionBundle:Aseg a 
                                           WHERE a.cod = '".$mat."'"
                                     );
        else
            $consul = $em->createQuery("SELECT b.cod, b.app, b.apm, b.nom FROM CpsAfiliacionBundle:Bnef b 
                                           WHERE b.cod = '".$mat."'"
                                     );
        return $consul->getSingleResult();
    }
    
    public function findPacXFiltro2($filtro){           
        $em = $this->getEntityManager();
        $w1 = "WHERE 1=1";
        if ($filtro != ''){
            $filtro = strtoupper($filtro);
            $agujas = explode(" ", $filtro);
            $cant = count($agujas);
            for ($i=0; $i<$cant; $i++){
                $w1 .= " AND (z.cod LIKE '%".$agujas[$i]."%'".
                       "   OR z.nom LIKE '%".$agujas[$i]."%'".
                       "   OR z.app LIKE '%".$agujas[$i]."%'".
                       "   OR z.apm LIKE '%".$agujas[$i]."%')"
                       ;
            }
        }

        $cBD = new ConexionBD('adm');
        $entities = $cBD->sql('SELECT z.cod, z.app, z.apm, z.nom, "" AS aseguradoid FROM asegurad:aseg z, labhc:efec_servaux r '.$w1.'
                                      AND r.cod_pac = z.cod
                               UNION
                               SELECT z.cod, z.app, z.apm, z.nom, "" AS aseguradoid FROM asegurad:aseg z, lab:efec_servaux r '.$w1.'
                                      AND r.cod_pac = z.cod
                               UNION
                               SELECT z.cod, z.app, z.apm, z.nom, "" AS aseguradoid FROM asegurad:aseg z, labsuc:efec_servaux r '.$w1.'
                                      AND r.cod_pac = z.cod
                               UNION
                               SELECT z.cod, z.app, z.apm, z.nom, z.cod_ase AS aseguradoid FROM asegurad:bnef z, labhc:efec_servaux r '.$w1.'
                                      AND r.cod_pac = z.cod
                               UNION
                               SELECT z.cod, z.app, z.apm, z.nom, z.cod_ase AS aseguradoid FROM asegurad:bnef z, lab:efec_servaux r '.$w1.'
                                      AND r.cod_pac = z.cod
                               UNION
                               SELECT z.cod, z.app, z.apm, z.nom, z.cod_ase AS aseguradoid FROM asegurad:bnef z, labsuc:efec_servaux r '.$w1.'
                                      AND r.cod_pac = z.cod
                               ORDER BY 2,3,4');
            
        return array('entities' => $entities);
    }
}
